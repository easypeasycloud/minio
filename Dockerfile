FROM golang:1.8.3-alpine3.5

MAINTAINER Minio Inc <dev@minio.io>

ENV GOPATH /go
ENV PATH $PATH:$GOPATH/bin
ENV CGO_ENABLED 0

WORKDIR /go/src/github.com/minio/

RUN  \
     apk update && \
     apk add --no-cache ca-certificates && \
     apk add --no-cache git musl-dev curl bash && \
     echo 'hosts: files mdns4_minimal [NOTFOUND=return] dns mdns4' >> /etc/nsswitch.conf && \
     go get -v -d github.com/minio/minio && \
     go get -v -d github.com/satori/go.uuid && \
     go get -v -d github.com/ventu-io/go-shortid && \ 
     go get -u github.com/minio/mc
     
     
COPY _shim/api-errors.go /go/src/github.com/minio/minio/cmd/api-errors.go

COPY _shim/bucket-handlers.go /go/src/github.com/minio/minio/cmd/bucket-handlers.go

COPY _shim/api-response.go /go/src/github.com/minio/minio/cmd/api-response.go

COPY _shim/bucket-policy.go /go/src/github.com/minio/minio/cmd/bucket-policy.go


RUN  \
     cd /go/src/github.com/minio/minio && \
     go install -v -ldflags "$(go run buildscripts/gen-ldflags.go)" && \
     rm -rf /go/pkg /go/src /usr/local/go

EXPOSE 9000

COPY buildscripts/docker-entrypoint.sh /usr/bin/

RUN chmod +x /usr/bin/docker-entrypoint.sh

ENTRYPOINT ["/usr/bin/docker-entrypoint.sh"]

VOLUME ["/export"]

CMD ["minio"]
